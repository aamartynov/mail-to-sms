# -*- coding: utf-8 -*-

{
    'name': 'Mail to SMS',
    'version': '10.0.18.01.10',
    'category': 'Phone, Email',
    'license': 'AGPL-3',
    'summary': "Send mail as SMS",
    'description': '''
        Add server action type - Send SMS.
        For send SMS used sms_devino module.
    ''',
    'author': "Martynov Alexander. Eyekraft company",
    'depends': ['sms_devino'],
    'external_dependencies': {
        'python': ['phonenumbers', 'lxml']
    },
    'data': [
        'mail_to_sms.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True
}
