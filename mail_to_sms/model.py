# -*- coding: utf-8 -*-

from odoo import models, api, fields
import phonenumbers
import logging
import lxml
from threading import Timer
from lxml.html import document_fromstring


logger = logging.getLogger(__name__)


def format_phone(phone, region='RU'):
    return phonenumbers.format_number(
        phonenumbers.parse(phone, region),
        phonenumbers.PhoneNumberFormat.E164
    )

def format_email_to(email_to_raw_str, separator=','):
    email_to_list = []

    for email_to in email_to_raw_str.split(separator):
        try:
            email_to_list.append(format_phone(email_to.strip()))
        except Exception:
            logger.exception(u'Eror while format to number: %s' % email_to)

    return separator.join(email_to_list)

class ServerActions(models.Model):
    _inherit = ['ir.actions.server']

    mail_to_sms_template_id = fields.Many2one(
        'mail.template',
        'SMS template',
        ondelete='set null',
        domain="[('model_id', '=', model_id)]",
        required=True
    )

    @api.model
    def _get_states(self):
        return super(ServerActions, self)._get_states() + [('mail_to_sms', 'Send mail to SMS')]

    @api.model
    def _try_run_action_mail_to_sms(self, action, eval_context):
        try:
            return self._do_run_action_mail_to_sms(action, eval_context)
        except Exception as exception:
            logger.exception(exception)

    @api.one
    def _do_run_action_mail_to_sms(self, action, eval_context):

        with api.Environment.manage():
            cursor = self.pool.cursor()
            environment = self.env(cr=cursor)
            self = self.with_env(environment)
            
            active_id = self._context.get('active_id')
            template = action.with_env(environment).mail_to_sms_template_id
            mail_values = template.generate_email(active_id)

            if not mail_values['email_to'] or not mail_values['body_html']:
                return

            to = format_email_to(mail_values.get('email_to'))
            content = document_fromstring(mail_values.get('body_html')).text_content()

            sms_values = {
                "to": to,
                "content": content
            }

            self.env['mail_to_sms.sms'].sudo().create(sms_values)

            cursor.commit()
            cursor.close()

    @api.model
    def run_action_mail_to_sms(self, action, eval_context=None):
        Timer(5, self._try_run_action_mail_to_sms, [action, eval_context]).start()


class Sms(models.Model):
    _name = 'mail_to_sms.sms'

    to = fields.Char(string='Sms')
    content = fields.Text(string='Sms content')

    @api.one
    def send(self):
        try:
            for to in self.to.split(','):
                self.env['sms_devino.sendandlog'].send_sms(to, self.content)
        except Exception as error:
            logger.exception("Error while try send sms")
        else:
            logger.info('sms to %s was send' % self.to)

        self.unlink()

    @api.model
    def process_sms_queue(self):
        """ For use from ir.crone """
        self.search([('to', '!=', 'False'), ('content', '!=', 'False')], limit=10).send()
